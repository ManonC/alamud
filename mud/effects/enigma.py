from .effect import Effect
from mud.events import EnigmaEvent

class EnigmaEffect(Effect):
    EVENT = EnigmaEvent
