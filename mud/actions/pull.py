from .action import Action2
from mud.events import PullEvent

class PullAction(Action2):
    EVENT = PullEvent
    ACTION = "pull"
    RESOLVE_OBJECT = "resolve_for_use"
