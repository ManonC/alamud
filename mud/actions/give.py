from .action import Action3
from mud.events import GiveOnEvent

class GiveOnAction(Action3):
    EVENT = GiveOnEvent
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_take"
    ACTION = "give-on"
