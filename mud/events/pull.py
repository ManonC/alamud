from .event import Event2

class PullEvent(Event2):
    NAME = "pull"

    def perform(self):
        if not self.object.has_prop("tirable"):
            self.fail()
            return self.inform("pull.failed")
        self.inform("pull")
