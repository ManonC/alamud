from .event import Event3

class GiveOnEvent(Event3):
    NAME = "give-on"

    def perform(self):
        if not self.object.has_prop("givable") or not self.object2.has_prop("giver"):
            self.fail()
            return self.inform("give-on.failed")

        self.object2.remove_prop("talkable")
        self.inform("give-on")
