from .event import Event1

class EnigmaEvent(Event1):
    NAME = "enigma"

    def perform(self):
        self.inform("enigma")
